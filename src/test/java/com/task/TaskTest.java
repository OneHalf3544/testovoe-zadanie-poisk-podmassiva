package com.task;

import org.junit.Test;
import org.junit.Assert;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class TaskTest {

    Task task = new Task();

    @Test(expected = IllegalArgumentException.class)
    public void nullTest() {
        // given
        int[] x = null;

        // run
        int[] y = task.getMaxSumSubarray(x);

        // assert : exception
    }

    @Test
    public void emptyTest() {
        // given
        int[] x = {};
        int[] y_exp = {};

        // run
        int[] y = task.getMaxSumSubarray(x);

        // assert
        Assert.assertArrayEquals(y_exp, y);
    }

    @Test
    public void fullTest() {
        // given
        int[] x = {1, 2, 3, 4, 5, 6};
        int[] y_exp = {1, 2, 3, 4, 5, 6};

        // run
        int[] y = task.getMaxSumSubarray(x);

        // assert
        Assert.assertArrayEquals(y_exp, y);
    }

    @Test
    public void nestedTest() {
        // given
        int[] x = {-3, 0, 0, 0, 1, 3, 4, 5, 6, 0, 0, 0};
        int[] y_exp = {0, 0, 0, 1, 3, 4, 5, 6, 0, 0, 0};

        // run
        int[] y = task.getMaxSumSubarray(x);

        // assert
        Assert.assertArrayEquals(y_exp, y);
    }

    @Test
    public void simpleTest() {
        // given
        int[] x = {-1, 3, 4, 5, -2, -5, -1};
        int[] y_exp = {3, 4, 5};

        // run
        int[] y = task.getMaxSumSubarray(x);

        // assert
        Assert.assertArrayEquals(y_exp, y);
    }

    @Test
    public void decisionTest() {
        // given
        int[] x = {-1, 3, 4, 5, -2, -5, -1, 10, 11, 12};
        int[] y_exp = {10, 11, 12};

        // run
        int[] y = task.getMaxSumSubarray(x);

        // assert
        Assert.assertArrayEquals(y_exp, y);
    }

    @Test
    public void singleTest() {
        // given
        int[] x = {-1, 3, -2, -5, -1};
        int[] y_exp = {3};

        // run
        int[] y = task.getMaxSumSubarray(x);

        // assert
        Assert.assertArrayEquals(y_exp, y);
    }

    @Test
    public void firstTest() {
        // given
        int[] x = {3, -2, -5, -1};
        int[] y_exp = {3};

        // run
        int[] y = task.getMaxSumSubarray(x);

        // assert
        Assert.assertArrayEquals(y_exp, y);
    }

    @Test
    public void lastTest() {
        // given
        int[] x = {-2, -5, -1, 3};
        int[] y_exp = {3};

        // run
        int[] y = task.getMaxSumSubarray(x);

        // assert
        Assert.assertArrayEquals(y_exp, y);
    }

    @Test
    public void tailTest() {
        // given
        int[] x = {-2, -5, -1, 1, 2, 3};
        int[] y_exp = {1, 2, 3};

        // run
        int[] y = task.getMaxSumSubarray(x);

        // assert
        Assert.assertArrayEquals(y_exp, y);
    }

    @Test
    public void duplicateTest() {
        // given
        int[] x = {-1, 3, 4, 5, -2, -5, -1, 3, 4, 5, -3};
        int[] y_exp = {3, 4, 5, -2, -5, -1, 3, 4, 5};

        // run
        int[] y = task.getMaxSumSubarray(x);

        // assert
        assertThat(y, is(y_exp));
    }

    @Test
    public void duplicateHeadTest() {
        // given
        int[] x = {3, 4, 5, -2, -5, -1, 3, 4, 5, -3};
        int[] y_exp = {3, 4, 5};

        // run
        int[] y = task.getMaxSumSubarray(x);

        // assert
        Assert.assertArrayEquals(y_exp, y);
    }

    @Test
    public void duplicateTailTest() {
        // given
        int[] x = {-1, 3, 4, 5, -2, -5, -1, 3, 4, 5};
        int[] y_exp = {3, 4, 5};

        // run
        int[] y = task.getMaxSumSubarray(x);

        // assert
        Assert.assertArrayEquals(y_exp, y);
    }

    @Test
    public void negativeOnlyTest() {
        // given
        int[] x = {-1, -2, -5, -3};
        int[] y_exp = {-1};

        // run
        int[] y = task.getMaxSumSubarray(x);

        // assert
        Assert.assertArrayEquals(y_exp, y);
    }

    @Test
    public void zeroTest() {
        // given
        int[] x = {-1, -2, 0, -5, -3};
        int[] y_exp = {0};

        // run
        int[] y = task.getMaxSumSubarray(x);

        // assert
        Assert.assertArrayEquals(y_exp, y);
    }

    @Test
    public void zeroSequenceTest() {
        // given
        int[] x = {-1, -2, 0, 0, 0, 0, -5, -3};
        int[] y_exp = {0, 0, 0, 0};

        // run
        int[] y = task.getMaxSumSubarray(x);

        // assert
        Assert.assertArrayEquals(y_exp, y);
    }

    @Test
    public void onePositiveElemTest() {
        // given
        int[] x = {1};
        int[] y_exp = {1};

        // run
        int[] y = task.getMaxSumSubarray(x);

        // assert
        Assert.assertArrayEquals(y_exp, y);
    }

    @Test
    public void oneNegativeElemTest() {
        // given
        int[] x = {-1};
        int[] y_exp = {-1};

        // run
        int[] y = task.getMaxSumSubarray(x);

        // assert
        Assert.assertArrayEquals(y_exp, y);
    }

    @Test
    public void sameSumElemTest() {
        // given
        int[] x = {-1, 2, 3, -1, 5, -2, 1, 4, -4};
        int[] y_exp = {2, 3};

        // run
        int[] y = task.getMaxSumSubarray(x);

        // assert
        Assert.assertArrayEquals(y_exp, y);
    }
}
