package com.task;

import java.util.Arrays;

public class Task {

    private int startBest;
    private int endBest;
    private int sumBest;

    private void checkBest(int start, int end, int sum) {
        if (sum > sumBest) {
            startBest = start;
            endBest = end;
            sumBest = sum;
        }
    }

    public int[] getMaxSumSubarray(int[] x) {
        if (x == null) {
            throw new IllegalArgumentException("Input array is null");
        }

        if (x.length == 0) {
            return new int[0];
        }

        int start = 0, end = 1, sum = x[0];

        startBest = start;
        endBest = end;
        sumBest = sum;

        for (; end < x.length; end++) {
            if (sum + x[end] < sum) {       // if current elem makes our summ worse
                checkBest(start, end, sum);
                start = end;
                sum = x[end];
            } else if (x[end - 1] < 0) {
                checkBest(start, end, sum);
                start = end;
                sum = x[end];
            } else {
                sum += x[end];
            }
        }
        checkBest(start, end, sum);

        return Arrays.copyOfRange(x, startBest, endBest);
    }

}
